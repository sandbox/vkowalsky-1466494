<?php
// $Id: page.tpl.php,v 1.00 2012/01/14 01:25:33 dries Exp $
?>
<?php
 drupal_add_js('sites/all/themes/agrosos/scripts/jquery.ui.core.js');
 drupal_add_js('sites/all/themes/agrosos/scripts/jquery.ui.datepicker.js');
?>

<div class="clearfix"  id="page-wrapper">
<div class="clearfix"  id="page">
	<!-- HEADER -->
	<div class="clearfix" id="header-wrapper">
		<div class="clearfix" id="header">			
			<div id="logo-container">
					<?php if ($logo): ?>
						<a id="logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
							<img alt="<?php print t('Home')?>" src="<?php print $logo; ?>">
						</a>
					<?php endif; ?>
			</div>	
				<!-- NAVIGATION -->
				<div id="navigation">
						<?php if ($main_menu):?>		
						<div id="main-menu">
						<?php print theme('links__system_main_menu', array('links' => $main_menu,'attributes' => array('id' => 'main-menu-links', 
						'class' => array('links', 'clearfix'),),'heading' => array('text' => t('Main menu'),'level' => 'h2', 'class' => array('element-invisible'),),)); ?>
						</div>
						<?php endif; ?>						
				</div>	
				<!-- NAVIGATION END -->
				<?php print render($page['header']); ?>										
			
		</div>	
	</div>
	<!-- HEADER END-->

	<!-- MAIN -->
	<div class="clearfix" id="main-wrapper">
		<div id="left-side">
				<div id="sidebar-first">
					<?php print render($page['sidebar_first']); ?>
				</div>		
				<?php if($secondary_menu):?>
								<div id="secondary-menu">
								<?php print theme('links__system_secondary_menu', array('links' => $secondary_menu,'attributes' => array('id' => 'secondary-menu-links',
								'class' => array('links', 'inline', 'clearfix'),),'heading' => array('text' => t('Secondary menu'),'level' => 'h2', 'class' => array('element-invisible'),),)); ?>
								</div>
				<?php endif; ?>
							
		</div>
		<div id="main-content">
				<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
				<?php if ($tabs):?><div id="tabs"><?php print render($tabs); ?></div><?php endif; ?>
				<?php print render($page['help']); ?>
				<?php if ($action_links):?><div id="action-links"><?php print render($action_links); ?></div><?php endif; ?>
				<?php print render($page['content']); ?>
				<?php  //print $feed_icons; ?>
				
		</div>
		<div id="sidebar-second">
			<?php print render($page['sidebar_second']); ?>
		</div>
		
	</div>
	<!-- MAIN END -->
	<!-- FOOTER -->
	<div id="footer-wrapper">
		<div id="footer" class="rounded-block">
		<?php print render($page['footer']); ?>
		</div>
	<div id="footer-bottom">
	
	</div>
	</div>
	<!-- FOOTER END -->
</div>
</div>   
